package com.qantas.steps;

import java.io.IOException;

import org.junit.Assert;

import com.qantas.pages.BasePage;
import com.qantas.pages.HomePage;
import com.qantas.pages.LoginPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Homepage_Step {
	  @Given("^that I navigate to the Home menu category$")
	    public void iNavigateToHomePage() throws Throwable {
		  new HomePage().navigateToHomePage();
	    }
	  
	  @And("^I am on the Homepage$")
	  public void iAmOnHomePAge()
	  {
		  Assert.assertTrue(new HomePage().iAmOnHomePage());
	  }
	  
	  @Then("^I should see all the information$")
	  public void iSeeAllInfo()
	  {
		  Assert.assertTrue(new HomePage().elementsInHomePageDisplayed());
	  }
	  
	  
}
