package com.qantas.steps;

import java.io.IOException;

import org.junit.Assert;

import com.qantas.pages.BasePage;
import com.qantas.pages.LoginPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login_Steps {
	  @Given("^that I navigate to the \"(.*)\" menu category$")
	    public void i_am_on_page(String item) throws Throwable {
		  new LoginPage().iAmOnTheLoginPage();
	    }
	  
	  @And("^I select the \"(.*)\" option$")
	  public void clickLoginButtonfromMenu(String item)
	  {
		  new LoginPage() .clickLoginInMenu();
	  }
	  
	
	  @And("^I am on the Login Page$")
	  public void iAmOnLoginPage()
	  {
		  new LoginPage().isUserNameDisplayed();
	  }
	  
	
	  
	  @Then("^I should see the login fields$") 
	  
		  public void isLoginFieldsPresent ()
		  {
	  Assert.assertTrue(new LoginPage().isUserNameDisplayed());
	  
	  Assert.assertTrue(new LoginPage().isPassWordDisplayed());
		  }
	  
	 

	   @And("^I should see the login button$")
		  public void loginBtnIsDisplayed()
		  {
			  new LoginPage().isLoginBunDisplayed();
		  }
	   
	   @When("^I type valid login credentials$")
	   public void validCredentialsEntered()
	   {
		   new LoginPage().enterValidCredentials();
	   }
	  
	   @Then("^I should see the login message \"(.*)\"$")
	   public void isLoginMessageDisplayed(String msg)
	   {
		   Assert.assertTrue( new LoginPage().loginMessageDisplayed(msg));
	   }

	   @And("^I should see the logout button with text \"(.*)\"$")
	   public void isLogoutBtnDisplayed(String msg)
	   {
		   Assert.assertTrue( new LoginPage().isLogoutDisplayed(msg));
	   }
	   
	   @And("^I take a screenshot$")
	   public void takeScreenShot() throws IOException
	   {
		   new BasePage().screenshot();
	   }
	   
	   @When ("^I press the \"(.*)\" button$")
	   public void clickLogout(String msg)
	   {
		   new LoginPage().clickLogout();
	   }
	   
	 @When("^I type invalid login credentials$")
	 public void enterInvalidCredentials()
	 {
		 new LoginPage().enterInValidCredentials();
	 }
	 
	 @Then("^I should see the error message \"(.*)\"$")
	 
	 public void iSeeErrorMsg(String msg)
	 {
		 Assert.assertTrue( new LoginPage().isErrorDisplayed(msg));
	 }
	 
	 @And("^I should see the try again button with text \"(.*)\"$")
	 public void isTryAgainBtnDisplayed(String msg)
	 {
		 Assert.assertTrue(new LoginPage().isTryAgainBtnDisplayed(msg));
	 }
	 
	 @When("^I press the try again button$")
	 public void clickTryAgainBtn()
	 {
		 new LoginPage().clickTryAgainBtn();
	 }
}
