package com.qantas.base;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.qantas.pages.BasePage;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Hook {
    @Before
    public void initializeTest() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("platformName", "Android");

		/* capabilities.setCapability("avd", "Nexus_5_API_26"); */
        File filepath = new File (System.getProperty("user.dir"));
        File appDir = new File (filepath,"src/test/java/sampleapk");
        File app = new File (appDir,"app-debug.apk");
       capabilities.setCapability("app", app.getAbsolutePath());
		
		  capabilities.setCapability("appPackage", "com.amazonaws.devicefarm.android.referenceapp");
		  capabilities.setCapability("appActivity", "com.amazonaws.devicefarm.android.referenceapp.Activities.MainActivity");
		 
        BasePage.driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    }

    @After
    public void tearDownTest(Scenario scenario) {
        if (scenario.isFailed()) try {
            byte[] screenshot =
                    ((TakesScreenshot) BasePage.driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }
        BasePage.driver.quit();
    }
}
