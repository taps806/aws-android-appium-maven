package com.qantas.pages;

import org.openqa.selenium.By;

import static org.hamcrest.MatcherAssert.assertThat;

public class LoginPage extends BasePage {
    
    
    
    By moreButn= By.xpath("//android.widget.ImageButton[@content-desc=\"ReferenceApp\"]");
  
    
    By loginButonInMenu= By.xpath("(//android.widget.TextView[@text=\"Login Page\"])");
    
    By username= By.id("com.amazonaws.devicefarm.android.referenceapp:id/username_text_input");
    
    By pwd= By.id("com.amazonaws.devicefarm.android.referenceapp:id/password_text_input");
    
    By loginBtn= By.id("com.amazonaws.devicefarm.android.referenceapp:id/login_button");
    
    By loginTextMessage = By.id("com.amazonaws.devicefarm.android.referenceapp:id/login_alt_message_textView");
   
   
    By tryAgainBtn= By.id("com.amazonaws.devicefarm.android.referenceapp:id/alt_button");
    		
    		By logoutBtn = By.id("com.amazonaws.devicefarm.android.referenceapp:id/alt_button");
    
    private static final String CORRECT_USER_NAME = "admin";
    private static final String CORRECT_PASSWORD = "password";
    
    private static final String INCORRECT_USER_NAME = "admin12";
    private static final String INCORRECT_PASSWORD = "password12";

    public void iAmOnTheLoginPage() {
		/*
		 * waitForVisibilityOf(btnTermsAndCondition,10);
		 * assertThat("Login with email page is not opened!",
		 * isElementPresent(driver.findElement(btnTermsAndCondition),5));
		 */
    	
    	driver.findElement(moreButn).click();
    	

    }

    public void clickLoginInMenu()
    {
    	
    	driver.findElement(loginButonInMenu).click();
    	
    }
    
    
    
    public boolean isUserNameDisplayed()
    {
    	if (driver.findElement(username).isDisplayed())
    	return true;
    	else
    		return false;
    }
    
    public boolean isPassWordDisplayed()
    {
    	if (driver.findElement(pwd).isDisplayed())
    	return true;
    	else
    		return false;
    }
    
    
    public boolean isLoginBunDisplayed()
    {
    	if (driver.findElement(loginBtn).isDisplayed())
    	return true;
    	else
    		return false;
    }
    
    
	/*
	 * public void enterUserNameandPassword(String userName, String password) {
	 * driver.findElement(username).sendKeys(userName);
	 * driver.findElement(pwd).sendKeys(password);
	 * driver.findElement(loginBtn).click(); }
	 */

    public void enterValidCredentials()
    {
    	driver.findElement(username).sendKeys(CORRECT_USER_NAME);
    	driver.findElement(pwd).sendKeys(CORRECT_PASSWORD);
    	driver.findElement(loginBtn).click();    
    	}
    
    
    public void enterInValidCredentials()
    {
    	driver.findElement(username).sendKeys(INCORRECT_USER_NAME);
    	driver.findElement(pwd).sendKeys(INCORRECT_PASSWORD);
    	driver.findElement(loginBtn).click();    
    	}
    public boolean loginMessageDisplayed(String msg)
    {
    	if (driver.findElement(loginTextMessage).getText().equals(msg))
    		return true;
    		else
    			return false;
    }
    public boolean isLogoutDisplayed(String msg)
    {
    	if (driver.findElement(logoutBtn).getText().equals(msg))
    		return true;
    		else
    			return false;
    }
    
    public void clickLogout()
    {
    	driver.findElement(logoutBtn).click();
    }
 
    
    public boolean isErrorDisplayed(String msg)
    {
    	if (driver.findElement(loginTextMessage).getText().equals(msg))
    		return true;
    		else
    			return false;
    }
    
    public boolean isTryAgainBtnDisplayed(String msg)
    {
    	if (driver.findElement(tryAgainBtn).getText().equals(msg))
    		return true;
    		else
    			return false;	
    	
    }
    
    public void clickTryAgainBtn()
    {
    	driver.findElement(tryAgainBtn).click();
    }
    
   
    

    
}
