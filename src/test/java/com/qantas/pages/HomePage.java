package com.qantas.pages;

import org.openqa.selenium.By;

import static org.hamcrest.MatcherAssert.assertThat;

public class HomePage extends BasePage {
  
    By homeButonInMenu= By.xpath("(//android.widget.TextView[@text=\"Home\"])");
    By moreButn= By.xpath("//android.widget.ImageButton[@content-desc=\"ReferenceApp\"]");
By homeHeadline = By.id("com.amazonaws.devicefarm.android.referenceapp:id/homepage_headline");

By homeSubHeadline = By.id("com.amazonaws.devicefarm.android.referenceapp:id/homepage_subheadline");
    public void navigateToHomePage() {
    	driver.findElement(moreButn).click();
    	driver.findElement(homeButonInMenu).click();
    }
    
    public boolean iAmOnHomePage()
    {
    	if(driver.findElement(homeHeadline).isDisplayed())
    		return true;
    	else
    		return false;
    }
    
    public boolean elementsInHomePageDisplayed()
    {
    	
    	if(driver.findElement(homeHeadline).isDisplayed() && driver.findElement(homeSubHeadline).isDisplayed() )
    		return true;
    	else
    		return false;
    	
    }
}
