package com.qantas.pages;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BasePage {
    public static AndroidDriver<MobileElement> driver;

    protected void waitForVisibilityOf(By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    protected boolean isElementPresent(MobileElement element, int timeout){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    public void screenshot() {
    	try
    	{
        File srcFile=driver.getScreenshotAs(OutputType.FILE);
        String filename=UUID.randomUUID().toString(); 
        File targetFile=new File("src/test/java/screenshot/" + filename +".jpg");
        FileUtils.copyFile(srcFile,targetFile);
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    	
    }
}
